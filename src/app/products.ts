export const products = [
  {
    name: 'Tracksuits',
    price: 18,
    description: 'In real life, Im always in tracksuits, and I never wear makeup. Teresa Palmer'
  },
  {
    name: 'Heels',
    price: 25,
    description: 'What you wear - and it always starts with your shoes - determines what kind of character you are.'
  },
  {
    name: 'Skinny Jeans',
    price: 15,
    description: 'When it comes to fashion, I love skinny jeans, a simple top, and a great handbag.'
  }
];


/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/