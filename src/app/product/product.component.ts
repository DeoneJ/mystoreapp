import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  @Input()
  description: string;
  @Input()
  price: number;
  @Input()
  name: string;

  displayPrice: boolean;

  showPrice() {
    this.displayPrice = true;
  }

  constructor() {}

  ngOnInit() {}
}
